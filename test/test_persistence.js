"use strict";

var Should;
var serialijse;
var Serialijse;

if (typeof require !== "undefined") {
  Should = require("should");
  Serialijse = require("../").Serialijse;
} else {
  Serialijse = serialijse.Serialijse;
  serialijse = null;
}

Should(true).eql(true);

serialijse = new Serialijse();

(function () {

  function Color(colorName) {
    this.name = colorName;
  }

  function Vehicule() {
    this.brand = "Fiat";
    this.price = 10000.05;
    this.color = new Color("blue");
    this.createdOn = new Date("04 May 1956 GMT");
  }

  serialijse.declarePersistable(Vehicule);
  serialijse.declarePersistable(Color);

  describe("persistence ", function () {

      it("should persist a simple javascript object", function (done) {
          var vehicule = { name: "GM" };

          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicule);
            done();
          });
        }
      );

      it("should persist a simple object", function (done) {
          var vehicule = new Vehicule();
          vehicule.brand = "Renault";
          vehicule.price = 95000;
          vehicule.createdOn = new Date("04 May 1949 22:00:00 GMT");
          vehicule.should.be.instanceOf(Vehicule);

          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicule);
            reconstructedObject.should.be.instanceOf(Vehicule);
            done();
          });
        }
      );

      it("should persist a simple containing an array", function (done) {
          var vehicule = new Vehicule();
          vehicule.passengers = ["Joe", "Jack"];

          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicule);
            done();
          });
        }
      );

      it("should persist a array of persistable object", function (done) {
          var vehicules = [new Vehicule(), new Vehicule()];
          vehicules[0].brand = "Renault";
          vehicules[0].price = 95000;
          vehicules[0].createdOn = new Date("Wed, 04 May 1949 22:00:00 GMT");

          serialijse.serialize(vehicules, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicules);
            done();
          });
        }
      );

      it("should persist a array containing two elements pointing to the same object ", function (done) {
          var theVehicule = new Vehicule();
          theVehicule.brand = "Citroen";
          theVehicule.price = 95000;
          theVehicule.createdOn = new Date("Wed, 04 May 1949 22:00:00 GMT");
          var vehicules = [theVehicule, theVehicule];
          vehicules[0].should.equal(vehicules[1]);

          serialijse.serialize(vehicules, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            Should(theVehicule.____index).eql(undefined);
            var expected = '[[' +
              '{"c":"Vehicule","d":{"brand":"Citroen","color":{"o":1},"createdOn":{"d":-651981600000},"price":95000}},' +
              '{"c":"Color","d":{"name":"blue"}}' +
              '],' + '{"a":[{"o":0},{"o":0}]}]';
            serializationString.should.eql(expected);
            //xx console.log(serializationString);
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.length.should.eql(2);
            reconstructedObject[0].should.equal(reconstructedObject[1]);
            reconstructedObject.should.eql(vehicules);
            done();
          });

        }
      );

      it("should not persist property defined in class prototype", function (done) {

          function Rectangle() {
            this.width = 10;
            this.height = 20;
            Object.defineProperty(
              this, "area", {
                get: function () {
                  return this.width * this.height;
                }
              }
            );
          }

          Rectangle.prototype.__defineGetter__(
            "perimeter", function () {
              return (this.width + this.height) * 2.0;
            }
          );
          var localSerialijse = new Serialijse();
          localSerialijse.declarePersistable(Rectangle);

          var rect1 = new Rectangle();
          rect1.width = 100;
          rect1.height = 2;
          rect1.area.should.equal(200);
          rect1.perimeter.should.equal(204);

          localSerialijse.serialize(rect1, function (err, serializationString) {
              if (err) {
                done(err);
                return;
              }
              var rect2 = localSerialijse.deserialize(serializationString);
              rect2.area.should.equal(200);
              rect2.perimeter.should.equal(204);
              done();
            }
          );
        }
      );

      it("testing compression impact ", function (done) {
          var vehicules = [new Vehicule(), new Vehicule(), new Vehicule()];

          serialijse.serialize(vehicules, function (err, uncompressedSerializationString) {
            if (err) {
              done(err);
              return;
            }
            serialijse.serializeZ(vehicules, function (err_, buffer) {
                //var serializationString = buffer.toString("base64");
                if (err_) {
                  done(err_);
                  return;
                }
                var compressionRatio = Math.round(100.0 - 100.0 * (buffer.length / uncompressedSerializationString.length));
                console.log(
                  "           = ", uncompressedSerializationString.length, "compressed =", buffer.length, " ratio ",
                  compressionRatio, "%"
                );
                serialijse.deserializeZ(buffer, function (err, reconstructedObject) {
                    if (err) {
                      done(err);
                      return;
                    }
                    reconstructedObject.should.eql(vehicules);
                    done();
                  }
                );
              }
            );
          });
        }
      );

      it("should persist object with boolean", function (done) {
          var vehicule = new Vehicule();
          vehicule.requireServicing = true;

          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicule);

            done();
          });
        }
      );

      it("should persist an object with a undefined property", function (done) {
          var vehicule = new Vehicule();
          vehicule.serviceDate = [null, new Date("2013/01/02")];

          // try to mess with the serialisation algo by adding a fake null property
          vehicule.toto = null;
          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            // delete it as it should not interfer
            delete vehicule.toto;
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.should.eql(vehicule);
            done();
          });

        }
      );

      it("should deserialize from an already parsed JSON string", function (done) {
          var vehicule = new Vehicule();

          serialijse.serialize(vehicule, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var jsonObj = JSON.parse(serializationString);
            var reconstructedObject = serialijse.deserialize(jsonObj);
            reconstructedObject.should.eql(vehicule);
            done();
          });

        }
      );

      it("unlike JSON.stringify/parse, it should serialize a standard object and preserve date", function (done) {
          var someObject = {
            theDate: new Date()
          };
          someObject.theDate.should.be.instanceOf(Date);

          serialijse.serialize(someObject, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var reconstructedObject = serialijse.deserialize(serializationString);
            reconstructedObject.theDate.should.be.instanceOf(Date);
            reconstructedObject.theDate.should.eql(someObject.theDate);
            reconstructedObject.should.eql(someObject);
            reconstructedObject.should.eql(someObject);
            done();
          });
        }
      );


      it("unlike JSON.stringify/parse, it should persist object that contains cyclic object value", function (done) {

          function Person(name) {
            this.name = name;
            this.parent = null;
            this.children = [];
          }

          serialijse.declarePersistable(Person);

          Person.prototype.addChild = function (name) {
            var child = new Person(name);
            child.parent = this;
            this.children.push(child);
            return child;
          };

          var mark = new Person("mark"),
            valery = mark.addChild("valery"),
            edgar = mark.addChild("edgar");

          valery.parent.should.equal(mark);
          edgar.parent.should.equal(mark);

          Should(
            function () {
              JSON.stringify(mark);
            }
          ).throwError();   // Circular

          serialijse.serialize(mark, function (err, serializationString) {
            if (err) {
              done(err);
              return;
            }
            var markReloaded = serialijse.deserialize(serializationString);
            markReloaded.name.should.eql("mark");
            markReloaded.children.length.should.eql(2);
            markReloaded.should.be.instanceOf(Person);
            markReloaded.children[0].should.be.instanceOf(Person);
            markReloaded.children[1].should.be.instanceOf(Person);
            markReloaded.children[0].parent.should.equal(markReloaded);
            markReloaded.children[1].parent.should.equal(markReloaded);
            markReloaded.children[0].name.should.eql("valery");
            markReloaded.children[1].name.should.eql("edgar");
            done();
          });
        }
      );
    }
  );

  describe('declareUnserializable', function () {

    var ser;

    beforeEach(function () {
      ser = new Serialijse();
    });

    it('adds given class name to unserializables prop', function () {
      ser.unserializables = [];
      ser.declareUnserializable(function TestClass() { });
      ser.unserializables.should.eql(['TestClass']);
    });

    it('prevents class from being serialized', function (done) {
      function UnserializableClass() { }
      function SerializableClass() { }
      ser.declareUnserializable(UnserializableClass);
      ser.declarePersistable(SerializableClass);
      ser.serialize({
        a: new UnserializableClass(),
        b: new SerializableClass()
      }, function (err, result) {
        if (err) {
          done(err);
          return;
        }

        result.should.eql('[[{"c":"Object","d":{"a":{"o":null},"b":{"o":1}}},' +
                          '{"c":"SerializableClass","d":{}}],{"o":0}]');

        var resultObj = ser.deserialize(result);

        resultObj['b'].should.be.instanceOf(SerializableClass);
        Should(resultObj['a']).eql(null);

        done();
      });
    });

  });
}());
