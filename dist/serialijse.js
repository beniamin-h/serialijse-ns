(function (exports) {

  "use strict";
  var assert = require("assert");
  var timers = require("timers");

  var isFunction = function (obj) {
    return typeof obj === 'function' || false;
  };

  function Serialijse() {
    this.gGlobal = {};
    this.unserializables = [];
  }

  Serialijse.prototype.gGlobal = null;

  Serialijse.prototype.declarePersistable = function (constructor) {
    var className = constructor.prototype.constructor.name;
    if (this.gGlobal.hasOwnProperty(className)) {
      console.warn("warning: declarePersistable : class " + className + " already registered");
    }
    //xx assert(!gGlobal.hasOwnProperty(className));
    this.gGlobal[className] = constructor;
  };

  Serialijse.prototype.declareUnserializable = function (constructor) {
    var className = constructor.prototype.constructor.name;
    if (this.unserializables.indexOf(className) > -1) {
      console.warn("warning: declareUnserializable : class " + className + " already registered as unserializable");
    }
    this.unserializables.push(className);
  };

  Serialijse.prototype.serialize = function (object, serializeCallback, stringifyFunc, eventEmitter) {

    stringifyFunc = typeof stringifyFunc === 'undefined' ? JSON.stringify : stringifyFunc;

    assert(object !== undefined, "serialize: expect a valid object to serialize ");

    var index = [];
    var objects = [];
    var that = this;

    function addObjectInIndex(obj, serializingData) {
      var id = index.length;
      obj.____index = id;
      index.push(serializingData);
      objects.push(obj);

      return id;
    }

    function findObject(obj) {
      if (obj.____index !== undefined) {
        assert(objects[obj.____index] === obj);
        return obj.____index;
      }
      return -1;
    }

    function serializeObject(serializingObject, object) {

      assert(object !== undefined);

      if (object === null) {
        serializingObject.o = null;
        return [];
      }

      var className = object.constructor.name;  // TODO: plain object (created by Object.create(null))

      // j => json object to follow
      // d => date
      // a => array
      // o => class  { c: className d: data }
      // o => null
      // @ => already serialized object

      if (className === "Array") {
        serializingObject.a = [];
        return object.map(
          function (elem, idx) {
            return [serializingObject.a, idx, elem];
          }
        );
      }
      if (className === "Date") {
        serializingObject.d = object.getTime();
        return [];
      }

      if (className !== "Object") {
        if (that.unserializables.indexOf(className) > -1) {
          serializingObject.o = null;
          return [];
        }
        if (!that.gGlobal.hasOwnProperty(className)) {
          throw new Error("class " + className + " is not registered in class Factory - deserialization will not be possible");
        }
      }

      // check if the object has already been serialized
      var id = findObject(object);

      var nextObjectsToSerialize = [];

      if (id === -1) { // not found
        // object hasn't yet been serialized
        var s = {
          c: className,
          d: {}
        };

        id = addObjectInIndex(object, s);

        Object.keys(object).sort().forEach(function (v) {
          if (v !== '____index') {
            if (object[v] !== null) {
              nextObjectsToSerialize.push([s.d, v, object[v]]);
            }
          }
        });

      }
      serializingObject.o = id;
      return nextObjectsToSerialize;
    }

    function _serialize(object_, callback) {

      var init = {};
      var nextObjectsToSerialize = [[init, 0, object_]];
      var failure = false;

      function _serializeProcess() {
        if (failure) {
          return;
        }

        var objectsToSerialize = nextObjectsToSerialize;
        nextObjectsToSerialize = [];

        objectsToSerialize.forEach(
          function (obj, idx) {
            timers.setImmediate(
              function () {
                try {
                  var value = obj[2];
                  var serializingObject = {};
                  switch (typeof value) {
                    case 'undefined':
                    case 'number':
                    case 'boolean':
                    case 'string':
                      // basic type
                      obj[0][obj[1]] = value;
                      break;
                    case 'object':
                      serializeObject(serializingObject, value, index).forEach(
                        function (nextObject) {
                          nextObjectsToSerialize.push(nextObject);
                        }
                      );
                      obj[0][obj[1]] = serializingObject;
                      if (eventEmitter) {
                        eventEmitter.emit('serializing', value.constructor.name);
                      }
                      break;
                    case 'function':
                      //do nothing - just omit
                      break;
                    default:
                      throw new Error("invalid typeof " + typeof value + " " + JSON.stringify(value, null, " "));
                  }

                  if (objectsToSerialize.length - 1 === idx) {
                    endProcess();
                  }
                } catch (err) {
                  serializeFail(err);
                }

              }
            );
          }
        );

      }

      function endProcess() {
        if (failure) {
          return;
        }
        if (!nextObjectsToSerialize.length) {
          callback(null, init[0]);
        } else {
          timers.setImmediate(
            function () {
              _serializeProcess();
            }
          );
        }
      }

      function serializeFail(err) {
        failure = true;
        callback(err);
      }

      _serializeProcess();
    }

    _serialize(
      object, function (err, obj) {
        // unset temporary ___index properties
        objects.forEach(
          function (e) {
            delete e.____index;
          }
        );

        if (!err) {
          serializeCallback(null, stringifyFunc([index, obj]));// ,null," ");
        } else {
          serializeCallback(err);
        }
      }
    );

  };

  Serialijse.prototype.deserialize = function (serializationString) {

    var data;
    var that = this;
    if (typeof serializationString === 'string') {
      data = JSON.parse(serializationString);
    } else if (typeof serializationString === 'object') {
      data = serializationString;
    }
    var index = data[0],
      obj = data[1],
      cache = [];

    function deserializeNodeOrValue(node) {
      if ("object" === typeof node) {
        return deserializeNode(node);
      }
      return { value: node, nextObjectsToDeserialize: [] };
    }

    function deserializeNode(node) {
      // special treatment
      if (!node) {
        return { value: null, nextObjectsToDeserialize: [] };
      }

      var nextObjectsToDeserialize;

      if (node.hasOwnProperty("d")) {
        return { value: new Date(node.d), nextObjectsToDeserialize: [] };
      }
      if (node.hasOwnProperty("j")) {
        return { value: node.j, nextObjectsToDeserialize: [] };
      }
      if (node.hasOwnProperty("o")) {
        var objectId = node.o;
        if (objectId === null) {
          return { value: null, nextObjectsToDeserialize: [] };
        }
        if (cache[objectId] !== undefined) {
          return { value: cache[objectId], nextObjectsToDeserialize: [] };
        }
        return deserializeObject(index[objectId], objectId);
      }
      if (node.hasOwnProperty("a")) {
        // return deserializeObject(node.o);
        var arr = [];
        nextObjectsToDeserialize = node.a.map(
          function (elem, idx) {
            return [arr, idx, elem];
          }
        );
        return { value: arr, nextObjectsToDeserialize: nextObjectsToDeserialize };
      }
      throw new Error("Unsupported deserializeNode" + JSON.stringify(node));
    }

    function deserializeObject(objectDefinition, objectId) {

      var constructor, resultObj, inputData, v, className;

      assert(objectDefinition.c);
      assert(objectDefinition.d);

      className = objectDefinition.c;
      inputData = objectDefinition.d;

      if (className === "Object") {
        resultObj = {};
      } else {
        constructor = that.gGlobal[className];
        if (!constructor) {
          throw new Error(" Cannot find constructor to deserialize class of type" + className + ". use declarePersistable(Constructor)");
        }
        assert(isFunction(constructor));
        resultObj = new constructor();
      }

      var nextObjectsToDeserialize = [];
      cache[objectId] = resultObj;
      for (v in inputData) {
        if (inputData.hasOwnProperty(v)) {
          nextObjectsToDeserialize.push([resultObj, v, inputData[v]]);
        }
      }
      return { value: resultObj, nextObjectsToDeserialize: nextObjectsToDeserialize };
    }

    var result = {};
    var objectsToDeserialize = [[result, 'value', obj]];
    var nextObjectsToDeserialize;
    while (objectsToDeserialize.length) {
      nextObjectsToDeserialize = [];
      objectsToDeserialize.forEach(function (objToDeser) {
          var result_ = deserializeNodeOrValue(objToDeser[2])
          objToDeser[0][objToDeser[1]] = result_.value;
          result_.nextObjectsToDeserialize.forEach(
            function (elem) {
              nextObjectsToDeserialize.push(elem);
            }
          );
        }
      );
      objectsToDeserialize = nextObjectsToDeserialize;
    }

    return result.value;
  };

  Serialijse.prototype.serializeZ = function (obj, callback) {
    var zlib = require("zlib");
    this.serialize(
      obj, function (serializeErr, str) {
        if (serializeErr) {
          return callback(null, serializeErr);
        }
        zlib.deflate(
          str, function (deflateErr, buff) {
            if (deflateErr) {
              return callback(deflateErr);
            }
            callback(null, buff);
          }
        );
      }
    );
  };

  Serialijse.prototype.deserializeZ = function (data, callback) {
    var zlib = require("zlib");
    var that = this;
    zlib.inflate(
      data, function (err, buff) {
        if (err) {
          return callback(err);
        }
        callback(null, that.deserialize(buff.toString()));
      }
    );
  };

  exports.Serialijse = Serialijse;

})(typeof exports === 'undefined' ? this.serialijse = {} : exports);